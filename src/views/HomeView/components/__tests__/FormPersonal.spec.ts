import { describe, it, expect, vitest, Mock } from "vitest";
import { mount } from "@vue/test-utils";
import FormPersonal from "../FormPersonal.vue";
import { errors } from "../../../../constants";
import { usePassportStore } from "../../../../stores/passport";

vitest.mock("pinia", () => ({
  createPinia: vitest.fn(),
  defineStore: vitest.fn(),
}));

const passportMock = {
  name: "BIRUT",
  surname: "BASANAVIIEN",
  document_id: "012345678",
  birth_date: "02-01-2019",
  personal_id: "49003111045",
  place_of_birth: "LIETUVA",
  expire_date: "02-01-2029",
  mrz: {
    line1: "P<LTUBASANAVICIENE<<BIRUTE<<<<<<<<<<<<<<<<<<",
    line2: "00000000<0LTU9003118F290102049003111045<<<86",
  },
  sex: "MOT/F",
};

vitest.mock("../../../../stores/passport", () => ({
  usePassportStore: vitest.fn(() => ({
    $state: {
      passport: passportMock,
    },
  })),
}));

describe("FormPersonal", () => {
  it("should show error message if personalIdLenght is less than 11", () => {
    const wrapper = mount(FormPersonal);
    (usePassportStore as unknown as Mock).mockImplementation(() =>
      vitest.fn(() => ({
        $state: {
          passport: {
            ...passportMock,
            personal_id: "123",
          },
        },
      }))
    );
    expect(wrapper.text()).toContain(errors.personalIdLength);
  });
});
