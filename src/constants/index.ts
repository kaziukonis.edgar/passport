export const errors = {
  lettersOnly: "Please input letters only",
  numbersOnly: "Please input numbers only",
  personalIdLength: "Personal Id should be 11 characters length",
  personalIdSexMatch: "Personal Id does not match sex (MOT/F - 4, VYR/M - 3)",
};

export const lettersRegex = /[a-zA-Z]/;
