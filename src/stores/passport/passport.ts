import { defineStore } from "pinia";
import type { Passport } from "./passport.type";
import passportImage from "../../assets/passport_image.png";

export const usePassportStore = defineStore({
  id: "passport",
  state: () => ({
    passport: {
      image: passportImage,
      name: "BIRUT",
      surname: "BASANAVIIEN",
      document_id: "012345678",
      birth_date: "02-01-2019",
      personal_id: "49003111045",
      place_of_birth: "LIETUVA",
      expire_date: "02-01-2029",
      mrz: {
        line1: "P<LTUBASANAVICIENE<<BIRUTE<<<<<<<<<<<<<<<<<<",
        line2: "00000000<0LTU9003118F290102049003111045<<<86",
      },
      sex: "MOT/F",
    },
  }),
  actions: {
    updatePassport(passportUpdated: Passport) {
      this.passport = {
        ...this.passport,
        ...passportUpdated,
      };
    },
  },
});
