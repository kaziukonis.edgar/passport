export interface Passport {
  name?: string;
  surname?: string;
  document_id?: string;
  birth_date?: string;
  personal_id?: string;
  place_of_birth?: string;
  expire_date?: string;
  mrz?: {
    line1: string;
    line2: string;
  };
  sex?: string;
}
